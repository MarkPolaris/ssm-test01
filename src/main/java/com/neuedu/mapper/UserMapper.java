package com.neuedu.mapper;

import com.neuedu.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: MARK
 * @Date: 2019/7/30 15:17
 * @Version: 1.0.0
 * @Description:
 */

@Repository
public interface UserMapper {
    List<User> findAll();

    void add(User user);

    void update(User user);

    void delete(Integer id);
}
