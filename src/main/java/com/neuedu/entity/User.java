package com.neuedu.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Author: MARK
 * @Date: 2019/7/29 19:32
 * @version: 1.0.0
 * @Description:
 */
@Data
public class User {
    private Integer id;
    @NotNull(message = "{user.username.null}")
    private String username;
    private Integer password;
    private Date birth;
}
