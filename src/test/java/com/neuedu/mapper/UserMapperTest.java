package com.neuedu.mapper;

import com.neuedu.AppTest;
import com.neuedu.entity.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 * @Author: MARK
 * @Date: 2019/7/30 15:24
 * @Version: 1.0.0
 * @Description:
 */
public class UserMapperTest extends AppTest {
    @Autowired
    private UserMapper userMapper;
    @Test
    public void testAdd(){
        User user = new User();
        user.setUsername("李四");
        user.setPassword(321);
        userMapper.add(user);
    }

}