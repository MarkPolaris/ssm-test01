#### 整合的两种方式

1. import 整合
   
   在springMVC配置文件中导入spring配置文件
   
   ```xml
   <!--导入spring配置-->
   <import resource="applicationContext.xml"/>
   ```
2. web.xml整合
   
   不修改springMVC和spring配置文件，在web.xml中整合
   
   ```xml
   <context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>classpath:applicationContext.xml</param-value>
   </context-param>
   <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
   </listener>
   ```
   
   ### 动静分离
   
   #### controller层
   
```java
package com.neuedu.controller;
import com.alibaba.fastjson.JSON;
import com.neuedu.entity.User;
import com.neuedu.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**

* @Author: MARK
* @Date: 2019/7/29 19:14
* @version: 1.0.0
* @Description:
  
  */

@Controller
@Slf4j
@ResponseBody
@RequestMapping(value = "/user" ,produces = "application/json; charset=utf-8")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/user")
    public String users(){
    
        log.info("-------用户列表controller进来了");
        List<User> list = userService.getAll();
        String json = JSON.toJSONString(list);
        return json;
    }
    
    @PostMapping("/add")
    public void add(@RequestParam("username") String username){
    
        log.info("-------add  controller");
        log.info("-------" + username);
        User user = new User();
        user.setUsername(username);
        userService.add(user);
    }
    
    @PostMapping("/update")
    public void update(HttpServletRequest request){
        log.info("-------update  controller");
        Integer id = Integer.parseInt(request.getParameter("modal_id"));
        String username = request.getParameter("modal_name");
        User user = new User();
        user.setId(id);
        user.setUsername(username);
        userService.update(user);
    }
    
    @PostMapping("/delete")
    public void delete(HttpServletRequest request){
        log.info("-------delete controller");
        Integer id = Integer.parseInt(request.getParameter("id"));
        userService.delete(id);
    }

}

```

#### AJAX
```javascript
// 提交更新
$("#modal_submit").click(function(){
    $.ajax({
        url: "../../../user/update", //请求路径urlPattern
        dataType: "text",
        type: "post",
        data: {
            id: $("#modal_id").text(),
            username: $("#modal_name").val(),
            birth: $("#modal_birth").val() + " 00:00:00"
        },
        success: function(){
            window.location.href="userList.html"
        }
    });
});
```

#### 配置后台数据校验通过validator

1. 配置pom.xml  注意使用5.0版本6.0报错
   
   ```xml
   <dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-validator</artifactId>
    <version>5.0.2.Final</version>
   </dependency>
   ```

```
2. spring-mvc.xml中配置validator
```xml
<!--配置validator-->
    <!-- 以下 validator ConversionService 在使用 mvc:annotation-driven会 自动注册-->
    <bean id="validator" class="org.springframework.validation.beanvalidation.LocalValidatorFactoryBean">
        <property name="providerClass" value="org.hibernate.validator.HibernateValidator"/>
        <!-- 如果不加默认到 使用classpath下的
       ValidationMessages.properties -->
        <property name="validationMessageSource" ref="messageSource"/>
    </bean>

    <!-- 国际化的消息资源文件（本系统中主要用于显示/错误消息定制） -->
    <bean id="messageSource" class="org.springframework.context.support.ReloadableResourceBundleMessageSource">
    <property name="basenames">
        <list>
            <!-- 在web环境中一定要定位到classpath 否则默认到当前web应用下找 -->
            <value>classpath:messages</value>
            <!--国际化配置文件，能否使用待定-->
            <!--<value>classpath:org.hibernate.validator.ValidationMessages</value>-->
        </list>
    </property>
    <property name="useCodeAsDefaultMessage" value="false"/>
    <property name="defaultEncoding" value="UTF-8"/>
    <property name="cacheSeconds" value="60"/>
    </bean>

    <!--注意添加validator-->
    <mvc:annotation-driven conversion-service="conversionService" validator="validator"/>
```

3. 编写messages.properties
   
   ```properties
   user.username.null=用户名不能为空
   ```

4. 实体类添加注解
   
   ```java
   @NotNull(message = "user.username.null")
    private String username;
   ```

5. controller添加注解
   
   ```java
   @PostMapping("/add")
    /**
     * @param [user] 实体类实现参数绑定
     * @return void
     * @create: 9:51 2019/8/1
     * @author MARK
     * @Description:
     */
    public void add(@Valid User user, BindingResult result){
        if (result.hasErrors()) {
            // 输出错误信息
            List<ObjectError> allErrors = result.getAllErrors();
            for (ObjectError objectError : allErrors) {
                // 输出错误信息
                log.info("-------"+objectError.getDefaultMessage());
            }
        }
        log.info("-------add  controller");
        log.info("-------" + user.getUsername());
        log.info("-------" + user.getBirth());
        log.info("-------" + user.getPassword());
        userService.add(user);
    }
   }
   ```

6. postman接口测试
   
   ```text
   [INFO ] 2019-08-01 10:57:19,899 method:com.neuedu.controller.UserController.add(UserController.java:62)
   -------用户名不能为空
   ```

7. 如果出现乱码，查看idea的编码，file-settings-editor-file Encoding  修改下面properties编码设置





#### json数据传递

1. 使用postman测试json接口

![5d4286d1d0c8d56978](https://i.loli.net/2019/08/01/5d4286d1d0c8d56978.png)

2. controller层

```java
@PostMapping("/json")
    public String getJson(@RequestBody String json){
        JSONObject userJson = JSONObject.parseObject(json);
        User user = JSON.toJavaObject(userJson, User.class);
        log.info("-------json.toJavaObject: "+user);
        return json;
    }
```

3. 日志输出

```textile
[INFO ] 2019-08-01 14:20:46,188 method:com.neuedu.controller.UserController.getJson(UserController.java:98)
-------json.toJavaObject: User(id=null, username=是否是, password=12121, birth=Wed Aug 07 00:00:00 CST 2019)
```
